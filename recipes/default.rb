#
# Cookbook Name:: balsam
# Recipe:: default
#
# Copyright 2019, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'Install Apache' do
                case node[:platform]
                when 'ubuntu'
                                package_name 'apache2'
                                action :install
                end
end

service 'apache2' do
                action [:enable, :start]
end

package 'Install MySQL' do
                case node[:platform]
                when 'ubuntu'
                                package_name 'mysql-server'
                                action :install
                end
end

service 'mysql' do
                action [:enable, :start]
end

file '/var/www/html/index.html' do
                content '<html>
                <head>
                <title> Balsam International</title>
                </head>
                <body>
                <h1> Hello Joseph Torres </h1>
                </body>
</html>'
end
