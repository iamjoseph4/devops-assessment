#!/bin/bash

if [[ ! -z "$1" && ! -z "$2" ]];
then
	mkdir -p /var/www/html/$1
	echo "Welcome to Balsam Brand" > /var/www/html/$1/$2
	echo "/var/www/html/$1 and file $2 created!";
else
	echo 'Invalid Argument! Please use "script.sh folder_name file_name" format'
fi
